#!/bin/bash
source /etc/profile

file='users'

cat ${file} | while read line
do

    arr=(${line})
    #sshpass -p '${arr[2]}' ssh-copy-id -i /root/.ssh/id_rsa.pub -o StrictHostKeyChecking=no ${arr[1]}@${arr[0]} 2> /dev/null
    /usr/bin/sshpass -p "${arr[2]}" /usr/bin/ssh-copy-id -i /root/.ssh/id_rsa.pub -o StrictHostKeyChecking=no "${arr[1]}@${arr[0]}" 2> /dev/null 1> /dev/null
    if [ $? == 0 ];then
        echo -e "主机: ${arr[0]} 添加key成功！"
    else
	echo -e ">>>>主机: ${arr[0]} 添加key失败！"
    fi
done
